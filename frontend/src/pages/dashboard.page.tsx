import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";
import Pagination from "../components/layout/pagination";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [currentUsers, setCurrentUsers] = useState<IUserProps[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(0);
  const [pages, setPages] = useState<number>(0);

  useEffect(() => {
    const fetchData = async () => {
      const result = await backendClient.getAllUsers();
      setUsers(result.data);
      setPages(result.data.length / 10);
      setCurrentPage(1);
      setLoading(false);
    };

    fetchData();
  }, []);

  useEffect(() => {
    const initial = currentPage * 10 - 10;
    setCurrentUsers(users.slice(initial, initial + 10));
  }, [currentPage, users]);

  const changePage = (n: number) => {
    setCurrentPage(n);
  };

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {currentUsers.length
              ? currentUsers.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
            <div>
              <Pagination
                pages={pages}
                currentPage={currentPage}
                changePage={changePage}
              />
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
