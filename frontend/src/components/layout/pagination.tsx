import React from "react";

interface PaginationProps {
  pages: number;
  currentPage: number;
  changePage: (n: number) => void;
}

const Pagination = ({ pages, currentPage, changePage }: PaginationProps) => {
  const handleClick = (n: number) => {
    changePage(n);
  };

  const paginationItems = Array(pages)
    .fill(0)
    .map((value, index) => (
      <button onClick={() => handleClick(index + 1)}>{index + 1}</button>
    ));

  return <div>{paginationItems}</div>;
};

export default Pagination;
